
INSERT INTO film (title, rental_rate, rental_duration)
VALUES ('My Favorite Film', 4.99, 14);


INSERT INTO actor (first_name, last_name)
VALUES ('Actor1', 'Lead1'),
       ('Actor2', 'Lead2'),
       ('Actor3', 'Lead3');


SET @actor1_id = LAST_INSERT_ID();
SET @actor2_id = LAST_INSERT_ID() - 1;
SET @actor3_id = LAST_INSERT_ID() - 2;

INSERT INTO film_actor (film_id, actor_id)
VALUES (LAST_INSERT_ID(), @actor1_id),
       (LAST_INSERT_ID(), @actor2_id),
       (LAST_INSERT_ID(), @actor3_id);

INSERT INTO inventory (film_id, store_id)
VALUES (LAST_INSERT_ID(), your_store_id);
